package com.example.misaki.myapp05;

import android.os.IBinder;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Deposit extends BaseProduct {
    @SerializedName("name")
    private String name;
    @SerializedName("percent")
    private double percent;
    @SerializedName("amount")
    private double amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("bonusPercent")
    private double bonusPercent;
    @SerializedName("openedDate")
    Date date;

    String getName() {
        return name;
    }

    double getPercent() {
        return percent;
    }

    double getAmount() {
        return amount;
    }

    String getCurrency() {
        return currency;
    }

    double getBonusPercent() {
        return bonusPercent;
    }

    @Override
    public String toString() {
        return name
                + " " + Double.toString(percent) + "%"
                + " " + Double.toString(amount)
                + " " + currency
                + " Opened:" + date.toString();
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDate() {
        return date;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPercent(double percent) {
        this.percent = percent;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setBonusPercent(double bonusPercent) {
        this.bonusPercent = bonusPercent;
    }
}
