package com.example.misaki.myapp05;

import com.google.gson.annotations.SerializedName;

public class Account extends BaseProduct {
    @SerializedName("percent")
    private double percent;
    @SerializedName("amount")
    private double amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("bonusPercent")
    private double bonusPercent;

    double getPercent() {
        return percent;
    }

    double getAmount() {
        return amount;
    }

    String getCurrency() {
        return currency;
    }

    double getBonusPercent() {
        return bonusPercent;
    }

}
