package com.example.misaki.myapp05;

public class ViewDebitCard {
    private String text = "<empty>";

    ViewDebitCard(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
