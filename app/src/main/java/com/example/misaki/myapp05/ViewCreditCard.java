package com.example.misaki.myapp05;

public class ViewCreditCard {
    private String text = "<empty>";

    ViewCreditCard(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
