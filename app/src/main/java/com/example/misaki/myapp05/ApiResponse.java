package com.example.misaki.myapp05;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class ApiResponse<T> {
    private ArrayList<String> productTypes;
    @SerializedName("resultCode")
    private int resultCode;
    @SerializedName("error")
    private String error;
    @SerializedName("result")
    private T result;

    int getResultCode() {
        return resultCode;
    }

    String getError() {
        return error;
    }

    T getResult() {
        return result;
    }

}
