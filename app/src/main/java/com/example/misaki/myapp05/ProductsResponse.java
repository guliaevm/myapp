package com.example.misaki.myapp05;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ProductsResponse {
    @SerializedName("accounts")
    private List<Credit> accounts;
    @SerializedName("deposits")
    private List<Deposit> deposits;
    @SerializedName("creditCards")
    private List<CreditCard> creditCards;
    @SerializedName("debitCards")
    private List<DebitCard> debitCards;
    @SerializedName("credits")
    private List<Credit> credits;

    int getAccountSize() {
        return accounts.size();
    }

    Credit getAccount(int index) {
        if (index >= 0 && index < accounts.size()) {
            return accounts.get(index);
        }
        return null;
    }

    int getDepositsSize() {
        if (deposits == null) {
            return 0;
        }
        return deposits.size();
    }

    Deposit getDeposit(int index) {
        if (deposits == null) {
            return null;
        }
        if (index >= 0 && index < deposits.size()) {
            return deposits.get(index);
        }
        return null;
    }

    int getCreditCardsSize() {
        if (creditCards == null) {
            return 0;
        }
        return creditCards.size();
    }

    CreditCard getCreditCard(int index) {
        if (creditCards == null) {
            return null;
        }
        if (index >= 0 && index < creditCards.size()) {
            return creditCards.get(index);
        }
        return null;
    }

    Credit getCredit(int index) {
        if (credits == null) {
            return null;
        }
        if (index >= 0 && index < credits.size()) {
            return credits.get(index);
        }
        return null;
    }

    int getDebitCardsSize() {
        if (debitCards == null) {
            return 0;
        }
        return debitCards.size();
    }

    DebitCard getDebitCard(int index) {
        if (debitCards == null) {
            return null;
        }
        if (index >= 0 && index < debitCards.size()) {
            return debitCards.get(index);
        }
        return null;
    }

    int getCreditsSize() {
        if (credits == null) {
            return 0;
        }
        return credits.size();
    }
}
