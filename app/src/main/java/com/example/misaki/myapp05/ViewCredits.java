package com.example.misaki.myapp05;

import java.util.ArrayList;

public class ViewCredits extends ViewProductBase {

    protected ArrayList<ViewCredit> credits;

    ViewCredits(ArrayList<ViewCredit> credits) {
        type = ViewProductBase.CREDIT;
        this.credits = credits;
    }

    String getText() {
        String s;
        if(credits==null) {
            s = "<null>";
        } else {
            s = Integer.toString(credits.size());
        }
        return "ViewCredits: " + s;
    }

    int getSize() {
        if (credits == null) {
            return 0;
        }
        return credits.size();
    }

    ViewCredit getCredit(int index) {
        if (credits == null) {
            return null;
        }
        if (index >= 0 && index < credits.size()) {
            return credits.get(index);
        }
        return null;
    }

}
