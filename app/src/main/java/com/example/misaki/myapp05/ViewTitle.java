package com.example.misaki.myapp05;

public class ViewTitle extends ViewProductBase{
    private String text = "<empty>";

    ViewTitle(String text) {
        type = ViewProductBase.TITLE;
        this.text = text;
    }

    String getText() {
        return text;
    }
}
