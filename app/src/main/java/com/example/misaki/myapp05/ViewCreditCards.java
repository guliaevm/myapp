package com.example.misaki.myapp05;

import java.util.ArrayList;

public class ViewCreditCards extends ViewProductBase {

    protected ArrayList<ViewCreditCard> creditCards;

    ViewCreditCards(ArrayList<ViewCreditCard> creditCards) {
        type = ViewProductBase.CREDIT_CARD;
        this.creditCards = creditCards;
    }

    String getText() {
        String s;
        if (creditCards == null) {
            s = "<null>";
        } else {
            s = Integer.toString(creditCards.size());
        }
        return "ViewCreditCards: " + s;
    }

    int getSize() {
        if (creditCards == null) {
            return 0;
        }
        return creditCards.size();
    }

    ViewCreditCard getCreditCard(int index) {
        if (creditCards == null) {
            return null;
        }
        if (index >= 0 && index < creditCards.size()) {
            return creditCards.get(index);
        }
        return null;
    }

}
