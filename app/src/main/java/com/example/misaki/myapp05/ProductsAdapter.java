package com.example.misaki.myapp05;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class ProductsAdapter extends RecyclerView.Adapter<ProductsAdapter.ViewHolder> {
    ProductsResponse products;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        private View productView;
        private TextView accountPercentTextView;
        private TextView accountAmountTextView;
        private TextView accountCurrencyTextView;
        private TextView accountBonusPercentTextView;

        public ViewHolder(View v) {
            super(v);
            productView = v;
            accountPercentTextView = v.findViewById(R.id.accountPercentTextView);
            accountAmountTextView = v.findViewById(R.id.accountAmountTextView);
            accountCurrencyTextView = v.findViewById(R.id.accountCurrencyTextView);
            accountBonusPercentTextView = v.findViewById(R.id.accountBonusPercentTextView);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public ProductsAdapter(ProductsResponse products) {
        this.products = products;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public ProductsAdapter.ViewHolder onCreateViewHolder(ViewGroup parent,
                                                         int viewType) {
        Context context = parent.getContext();
        LayoutInflater layoutInflater = LayoutInflater.from(context);
        View v = layoutInflater.inflate(R.layout.product_view, parent, false);
        ViewHolder vh = new ViewHolder(v);
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Credit account = products.getAccount(position);
        holder.accountPercentTextView.setText(Double.toString(account.getPercent()));
        holder.accountAmountTextView.setText(Double.toString(account.getAmount()));
        holder.accountCurrencyTextView.setText(account.getCurrency());
        holder.accountBonusPercentTextView.setText(Double.toString(account.getBonusPercent()));
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return products.getAccountSize();
    }
}

