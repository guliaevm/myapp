package com.example.misaki.myapp05;

import com.google.gson.annotations.SerializedName;

public class Credit extends BaseProduct {
    @SerializedName("name")
    private String name;
    @SerializedName("percent")
    private double percent;
    @SerializedName("amount")
    private double amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("bonusPercent")
    private double bonusPercent;

    double getPercent() {
        return percent;
    }

    double getAmount() {
        return amount;
    }

    String getCurrency() {
        return currency;
    }

    double getBonusPercent() {
        return bonusPercent;
    }

    @Override
    public String toString() {
        return name
                + " " + Double.toString(percent) + "%"
                + " " + Double.toString(amount)
                + " " + currency;
    }

}
