package com.example.misaki.myapp05;

import java.util.ArrayList;
import java.util.List;

public class ViewResponseObjects {
    private ArrayList<ViewProductBase> products;

    ViewResponseObjects(ApiResponse<ProductsResponse> apiResponse) {
        products = new ArrayList<>();
        fillProducts(apiResponse);
    }


    ViewResponseObjects(List<Deposit> deposits) {
        products = new ArrayList<>();
        fillDeposits(deposits);
    }

    // вклады, кредиты, карты, закрытые продукты
    void fillProducts(ApiResponse<ProductsResponse> apiResponse) {
        ProductsResponse result = apiResponse.getResult();
        if (result.getAccountSize() > 0) {
            //productTypes.add("Accounts");
        }
        if (result.getDepositsSize() > 0) {
            products.add(new ViewTitle("Deposits"));
            ArrayList<ViewDeposit> viewDepositsList = new ArrayList<>();
            for(int i = 0; i < result.getDepositsSize(); i++) {
                Deposit deposit = result.getDeposit(i);
                String s = deposit.toString();
                ViewDeposit viewDeposit = new ViewDeposit(s);
                viewDepositsList.add(viewDeposit);
            }
            products.add(new ViewDeposits(viewDepositsList));
        }
        if (result.getCreditsSize() > 0) {
            products.add(new ViewTitle("Credits"));
            ArrayList<ViewCredit> viewCreditsList = new ArrayList<>();
            for(int i = 0; i < result.getCreditsSize(); i++) {
                Credit credit = result.getCredit(i);
                String s = credit.toString();
                ViewCredit viewCredit = new ViewCredit(s);
                viewCreditsList.add(viewCredit);
            }
            products.add(new ViewCredits(viewCreditsList));
        }
        if (result.getCreditCardsSize() > 0) {
            products.add(new ViewTitle("Credit Cards"));
            ArrayList<ViewCreditCard> viewCreditCardsList = new ArrayList<>();
            for(int i = 0; i < result.getCreditCardsSize(); i++) {
                CreditCard creditCard = result.getCreditCard(i);
                String s = creditCard.toString();
                ViewCreditCard viewCreditCard = new ViewCreditCard(s);
                viewCreditCardsList.add(viewCreditCard);
            }
            products.add(new ViewCreditCards(viewCreditCardsList));
        }
        if (result.getDebitCardsSize() > 0) {
            products.add(new ViewTitle("Debit Cards"));
            ArrayList<ViewDebitCard> viewDebitCardsList = new ArrayList<>();
            for(int i = 0; i < result.getDebitCardsSize(); i++) {
                DebitCard debitCard = result.getDebitCard(i);
                String s = debitCard.toString();
                ViewDebitCard viewDebitCard = new ViewDebitCard(s);
                viewDebitCardsList.add(viewDebitCard);
            }
            products.add(new ViewDebitCards(viewDebitCardsList));
        }
    }

    void fillDeposits(List<Deposit> deposits) {
        if (deposits.size() > 0) {
            products.add(new ViewTitle("Deposits"));
            ArrayList<ViewDeposit> viewDepositsList = new ArrayList<>();
            for(int i = 0; i < deposits.size(); i++) {
                Deposit deposit = deposits.get(i);
                String s = deposit.toString();
                ViewDeposit viewDeposit = new ViewDeposit(s);
                viewDepositsList.add(viewDeposit);
            }
            products.add(new ViewDeposits(viewDepositsList));
        }
    }

    int getProductTypesSize() {
        return products.size();
    }

    ViewProductBase getProduct(int index) {
        if (index >= 0 && index < products.size()) {
            return products.get(index);
        }
        return null;
    }

}
