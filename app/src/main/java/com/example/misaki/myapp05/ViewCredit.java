package com.example.misaki.myapp05;

import android.view.View;

public class ViewCredit {
    private String text = "<empty>";

    ViewCredit(String text) {
        this.text = text;
    }

    String getText() {
        return text;
    }
}
