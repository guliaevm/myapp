package com.example.misaki.myapp05;

import java.util.ArrayList;

public class ViewDeposits extends ViewProductBase {

    protected ArrayList<ViewDeposit> deposits;

    ViewDeposits(ArrayList<ViewDeposit> deposits) {
        type = ViewProductBase.DEPOSIT;
        this.deposits = deposits;
    }

    String getText() {
        String s;
        if (deposits == null) {
            s = "<null>";
        } else {
            s = Integer.toString(deposits.size());
        }
        return "ViewDeposits: " + s;
    }

    int getSize() {
        if (deposits == null) {
            return 0;
        }
        return deposits.size();
    }

    ViewDeposit getDeposit(int index) {
        if (deposits == null) {
            return null;
        }
        if (index >= 0 && index < deposits.size()) {
            return deposits.get(index);
        }
        return null;
    }

}
