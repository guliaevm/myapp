package com.example.misaki.myapp05;

import com.google.gson.annotations.SerializedName;

public class CreditCard extends BaseProduct {
    @SerializedName("name")
    private String name;
    @SerializedName("percent")
    private double percent;
    @SerializedName("amount")
    private double amount;
    @SerializedName("currency")
    private String currency;
    @SerializedName("bonusPercent")
    private double bonusPercent;
    @SerializedName("cardNumber")
    private String cardNumber;

    double getPercent() {
        return percent;
    }

    double getAmount() {
        return amount;
    }

    String getCurrency() {
        return currency;
    }

    double getBonusPercent() {
        return bonusPercent;
    }

    @Override
    public String toString() {
        String card_type = "";
        if (cardNumber != null && cardNumber.length() > 0) {
            if (cardNumber.charAt(0) == '4') {
                card_type = " VISA";
            } else if (cardNumber.charAt(0) == '5') {
                card_type = " MAESTRO";
            }
        }
        return name
                + card_type
                + " " + Double.toString(percent) + "%"
                + " " + Double.toString(amount)
                + " " + currency;
    }

}
