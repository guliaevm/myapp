package com.example.misaki.myapp05;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;

class MyLooperThread extends Thread {

    public Handler handler;

    public void run() {
        Looper.prepare();
        handler = new Handler() {
            @Override
            public void handleMessage(Message msg) {
                // обработка поступившего Message
            }

        };
        Looper.loop();
    }
}

