package com.example.misaki.myapp05;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

import com.google.gson.Gson;

public class ResultShowActivity extends AppCompatActivity {

    private String jsonString;

    ApiResponse<ProductsResponse> apiResponse;
    ProductsResponse productsResponse;

    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_result_show);

        Intent intent = getIntent();
        jsonString = intent.getStringExtra("Response");
        apiResponse = ApiResponseFromJson(jsonString);
        productsResponse = apiResponse.getResult();

        recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        // specify an adapter (see also next example)
        //adapter = new ProductsAdapter(productsResponse);
        ViewResponseObjects products = new ViewResponseObjects(apiResponse);
        adapter = new ViewProductsAdapter(products);
        recyclerView.setAdapter(adapter);
    }

    ApiResponse<ProductsResponse> ApiResponseFromJson(String s) {
        Gson gson = new Gson();
        return apiResponse = gson.fromJson(jsonString, ApiResponseFromProductsResponse.class);
    }
}
