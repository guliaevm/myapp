package com.example.misaki.myapp05;

import retrofit2.Call;
import retrofit2.http.GET;

public interface BankApi {

    @GET("products")
    Call<ApiResponse<ProductsResponse>> getProducts();
}

