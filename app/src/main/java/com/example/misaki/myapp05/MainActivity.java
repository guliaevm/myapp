package com.example.misaki.myapp05;

import android.os.Build;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import com.google.gson.Gson;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.urlEditText)
    EditText urlEditText;
    @BindView(R.id.requestButton)
    Button requestButton;
    @BindView(R.id.requestDefButton)
    Button requestDefButton;
    private int progressCount = 0;

    @BindView(R.id.progressTestButton)
    Button progressTestButton;

    @BindView(R.id.progressBarHorizontal)
    ProgressBar progressBarHorizontal;

    LooperThread looperThread;

    private static Handler mainUIHandler;

    protected class DepositSortByDateAmountComparator implements Comparator<Deposit> {
        @Override
        public int compare(Deposit left, Deposit right) {
            int res1 = left.getDate().compareTo(right.getDate());
            if(res1 != 0) {
                return res1;
            }
            if(left.getAmount() == right.getAmount()) {
                return 0;
            }
            if(left.getAmount() > right.getAmount()) {
                return 1;
            }
            return -1;
        }
    }

    private class MyRunnable implements Runnable {
        static final int numMaxDef = 100;

        private int value;
        private int valueMax;
        List<Deposit> deposits;

        MyRunnable(int value, int valueMax, List<Deposit> deposits) {
            this.value = value;
            this.valueMax = valueMax;
            this.deposits = deposits;
        }

        @Override
        public void run() {
            // код для исполнения в главном потоке
            progressTestButton.setText("Ok:" + Integer.toString(value));
            progressBarHorizontal.setMax(valueMax);
            progressBarHorizontal.setProgress(value);
            if (value == 0) {
                progressBarHorizontal.setVisibility(View.VISIBLE);
            } else if (value == valueMax) {
                progressBarHorizontal.setVisibility(View.INVISIBLE);
                if (deposits != null) {
                    Collections.sort(deposits, new DepositSortByDateAmountComparator());
                    if (deposits.size() <= numMaxDef) {
                        depositsToRecyclerView(deposits);
                    } else {
                        depositsToRecyclerView(deposits.subList(0, numMaxDef));
                    }
                }
            }
        }
    }

    private class LooperThread extends Thread {

        static final int numDef = 1000;

        Handler handler;

        public void run() {
            Looper.prepare();
            handler = new Handler() {
                @Override
                public void handleMessage(Message msg) {
                    if (msg.what == 0) {
                        String[] names = {"Магнит", "Аптека", "Банк", "Айкай"};
                        List<Deposit> deposits = new ArrayList<>();

                        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");

                        Handler h = new Handler(Looper.getMainLooper());
                        Message message0 = Message.obtain(h, new MyRunnable(0, numDef, null));
                        message0.what = 0;
                        message0.sendToTarget();

                        for (int i = 0; i < numDef; i++) {
                            Deposit deposit = new Deposit();
                            deposit.setName(names[i % names.length]);
                            int daysBack = (int) Math.round(Math.random() * 365.0);
                            Calendar c = Calendar.getInstance();
                            c.add(Calendar.DATE, -daysBack);
                            int day = c.get(Calendar.DAY_OF_MONTH);
                            int month = c.get(Calendar.MONTH);
                            int year = c.get(Calendar.YEAR);
                            String dateStr = Integer.toString(year)
                                    + "-" + Integer.toString(month)
                                    + "-" + Integer.toString(day);
                            Date date = null;
                            try {
                                date = format.parse(dateStr);
                            } catch (ParseException e) {

                            }
                            deposit.setDate(date);
                            deposit.setAmount((int) Math.round(Math.random() * 1000.0));
                            deposits.add(deposit);

                            Message message = Message.obtain(h, new MyRunnable(i, numDef, null));
                            message.what = i;
                            message.sendToTarget();
                        }

                        Message message100 = Message.obtain(h, new MyRunnable(numDef, numDef, deposits));
                        message100.what = numDef;
                        message100.sendToTarget();
                    }
                }
            };
            Looper.loop();
        }

        Handler getHandler() {
            return handler;
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        looperThread = new LooperThread();
        looperThread.start();

        mainUIHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                // TODO Auto-generated method stub
                //progressTestButton.setText("Ok:" + Integer.toString(message.what));
                int v = message.what;
            }
        };

    }

    @Override
    protected void onDestroy() {
        looperThread.getHandler().getLooper().quit();
        super.onDestroy();
    }

    @OnClick(R.id.requestButton)
    public void onRequestButtonClick(View v) {
        retrofitAction();
    }

    @OnClick(R.id.requestDefButton)
    public void onRequestDefButtonClick(View v) {
        urlEditText.setText(R.string.url_def);
        Gson gson = new Gson();
        ApiResponse<ProductsResponse> apiResponse = gson.fromJson(ResponseMock.responseOrg, ApiResponseFromProductsResponse.class);
        productsToRecyclerView(apiResponse);
    }

    @OnClick(R.id.progressTestButton)
    void onProgressTestButtonClick(View v) {
        progressTestButton.setText("Start");
        if (looperThread.getHandler() != null) {
            Message message = looperThread.getHandler().obtainMessage(0);
            message.sendToTarget();
        }
    }

    void retrofitAction() {
        final HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BASIC);
        final OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(logging)
                .build();

        final Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://www.mocky.io/v2/5b21590730000091265c7459/")
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        BankApi bankApi = retrofit.create(BankApi.class);

        Call<ApiResponse<ProductsResponse>> call = bankApi.getProducts();

        call.enqueue(new Callback<ApiResponse<ProductsResponse>>() {

            @Override
            public void onResponse(@NonNull Call<ApiResponse<ProductsResponse>> call,
                                   @NonNull Response<ApiResponse<ProductsResponse>> response) {
                ApiResponse<ProductsResponse> apiResponse = response.body();
                ProductsResponse productsResponse = apiResponse.getResult();
                productsToRecyclerView(apiResponse);
            }

            @Override
            public void onFailure(@NonNull Call<ApiResponse<ProductsResponse>> call, @NonNull Throwable t) {
                Log.e("DEXSYS", t.getMessage());
            }

        });

    }

    private void productsToRecyclerView(ApiResponse<ProductsResponse> apiResponse) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ViewResponseObjects products = new ViewResponseObjects(apiResponse);
        RecyclerView.Adapter adapter = new ViewProductsAdapter(products);
        recyclerView.setAdapter(adapter);
    }

    private void depositsToRecyclerView(List<Deposit> deposits) {
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.my_recycler_view);
        recyclerView.setHasFixedSize(true);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        ViewResponseObjects products = new ViewResponseObjects(deposits);
        RecyclerView.Adapter adapter = new ViewProductsAdapter(products);
        recyclerView.setAdapter(adapter);
    }

}
